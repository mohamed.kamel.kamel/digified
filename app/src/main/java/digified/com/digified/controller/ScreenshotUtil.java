package digified.com.digified.controller;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Environment;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class ScreenshotUtil {

    public static Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile();
            if (pictureFile == null) {
                return;
            }
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    private static File getOutputMediaFile() {
        String state = Environment.getExternalStorageState();
        if (!state.equals(Environment.MEDIA_MOUNTED)) {
            return null;
        }
        else {
            File folder_gui = new File(Environment.getExternalStorageDirectory() + File.separator + "digified");
            if (!folder_gui.exists()) {
                folder_gui.mkdirs();
            }
            File outFile = new File(folder_gui, Calendar.getInstance().getTimeInMillis() + "digified.png");
            return outFile;
        }
    }

    public static Bitmap takeScreenShot(Activity activity, int width, int height) {
        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap b1 = view.getDrawingCache().copy(Bitmap.Config.RGB_565, false);

//        Rect frame = new Rect();
//        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
//        int statusBarHeight = frame.top;
//
//        Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - statusBarHeight);
        view.setDrawingCacheEnabled(false);
//        view.destroyDrawingCache();
        return b1;
    }

    public static void savePic(Bitmap b, String strFileName) {
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(strFileName);
            b.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void applyFlagSecure(Activity activity, boolean flagSecure) {
        Window window = activity.getWindow();
        WindowManager wm = activity.getWindowManager();

        int flags = window.getAttributes().flags;
        if (flagSecure && (flags & WindowManager.LayoutParams.FLAG_SECURE) != 0) {
            return;
        } else if (!flagSecure && (flags & WindowManager.LayoutParams.FLAG_SECURE) == 0) {
            return;
        }

        boolean flagsChanged = false;
        if (flagSecure) {
            window.addFlags(WindowManager.LayoutParams.FLAG_SECURE);
            flagsChanged = true;
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
            flagsChanged = true;
        }

        if (flagsChanged && ViewCompat.isAttachedToWindow(window.getDecorView())) {
            wm.removeViewImmediate(window.getDecorView());
            wm.addView(window.getDecorView(), window.getAttributes());
        }
    }
}
