package digified.com.digified.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import digified.com.digified.R;
import digified.com.digified.model.network.webservice;


public class SplashActivity extends AppCompatActivity {
    private TextView inHint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        inHint = findViewById(R.id.hint);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!webservice.isNetworkAvailable(SplashActivity.this)) {
                    DigifiedApplication.showInfoDialog(SplashActivity.this, getResources().getString(R.string.connectivity_error),
                            getResources().getString(R.string.internet_hint));
                    return;
                } else {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    SplashActivity.this.finish();
                }

            }
        }, 2000);
    }
}
