package digified.com.digified.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import digified.com.digified.R;
import digified.com.digified.controller.ScreenshotUtil;

public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback {
    SurfaceView cameraView, transparentView;
    SurfaceHolder holder, holderTransparent;
    Camera camera;
    private float RectLeft, RectTop, RectRight, RectBottom;
    int deviceHeight, deviceWidth;
    private TextView hintView;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getPermissions()) return;

        ScreenshotUtil.applyFlagSecure(this, true);

        setContentView(R.layout.activity_main);

        transparentView = (SurfaceView) findViewById(R.id.TransparentView);
        holderTransparent = transparentView.getHolder();
        holderTransparent.addCallback((SurfaceHolder.Callback) this);
        holderTransparent.setFormat(PixelFormat.TRANSLUCENT);
        transparentView.setZOrderMediaOverlay(true);

        cameraView = (SurfaceView) findViewById(R.id.CameraView);
        holder = cameraView.getHolder();
        holder.addCallback((SurfaceHolder.Callback) this);

        cameraView.setSecure(true);
        deviceWidth = getScreenWidth();
        deviceHeight = getScreenHeight();

        hintView = new TextView(this);
        hintView.setText(getResources().getString(R.string.scanCopy));
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(75, (int) (deviceHeight * 0.1), 75, 10);
        hintView.setLayoutParams(lp);
        hintView.setTextColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
        hintView.setTextSize((float) 18.0);
        hintView.setGravity(Gravity.CENTER_HORIZONTAL);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.scrollView1);
        relativeLayout.addView(hintView);
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    private void Draw() {
        Canvas canvas = holderTransparent.lockCanvas(null);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ResourcesCompat.getColor(getResources(), R.color.grayTransparent, null));
        RectLeft = 0;
        RectTop = 0;
        RectRight = deviceWidth;
        RectBottom = (float) (deviceHeight * 0.20);
        Rect rec = new Rect((int) RectLeft, (int) RectTop, (int) RectRight, (int) RectBottom);
        canvas.drawRect(rec, paint);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ResourcesCompat.getColor(getResources(), R.color.grayTransparent, null));
        RectLeft = 0;
        RectTop = (float) (deviceHeight * 0.55);
        RectRight = deviceWidth;
        RectBottom = deviceHeight;
        rec = new Rect((int) RectLeft, (int) RectTop, (int) RectRight, (int) RectBottom);
        canvas.drawRect(rec, paint);

        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.GREEN);
        RectLeft = 50;
        RectTop = (float) (deviceHeight * 0.20);
        RectRight = deviceWidth - 50;
        RectBottom = (float) (deviceHeight * 0.55);
        rec = new Rect((int) RectLeft, (int) RectTop, (int) RectRight, (int) RectBottom);
        canvas.drawRect(rec, paint);

        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ResourcesCompat.getColor(getResources(), R.color.grayTransparent, null));
        RectLeft = 0;
        RectTop = (float) (deviceHeight * 0.20);
        RectRight = 50;
        RectBottom = (float) (deviceHeight * 0.55);
        rec = new Rect((int) RectLeft, (int) RectTop, (int) RectRight, (int) RectBottom);
        canvas.drawRect(rec, paint);

        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ResourcesCompat.getColor(getResources(), R.color.grayTransparent, null));
        RectLeft = deviceWidth - 50;
        RectTop = (float) (deviceHeight * 0.20);
        RectRight = deviceWidth;
        RectBottom = (float) (deviceHeight * 0.55);
        rec = new Rect((int) RectLeft, (int) RectTop, (int) RectRight, (int) RectBottom);
        canvas.drawRect(rec, paint);

        holderTransparent.unlockCanvasAndPost(canvas);
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        synchronized (holder) {
            Draw();
        }
        if (!getCameraDevice(holder)) return;
        try {
            Thread.sleep(5000);
            captureScreen();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean getCameraDevice(SurfaceHolder holder) {
        try {
            camera = Camera.open();
        } catch (Exception e) {
            Log.i("Exception", e.toString());
            return true;
        }
        Camera.Parameters param;
        param = camera.getParameters();
//        param.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH); //To set flash on
        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        if (display.getRotation() == Surface.ROTATION_0) {
            camera.setDisplayOrientation(90);
        }
        camera.setParameters(param);
        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        } catch (Exception e) {
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
////        boolean isCapturedCorrect = false;
////        while (!isCapturedCorrect) {
////            try {
////                Thread.sleep(5000);
////                captureScreen();
////            } catch (InterruptedException e) {
////                e.printStackTrace();
////            }
////        }
//    }

    private void captureScreen() {
        try {
//            File cacheDir = new File(
//                    android.os.Environment.getExternalStorageDirectory(),
//                    "digified");
//            if (!cacheDir.exists()) {
//                cacheDir.mkdirs();
//            }
//            String path = new File(
//                    android.os.Environment.getExternalStorageDirectory(),
//                    "digified") + "/" + Calendar.getInstance().getTimeInMillis() + "digified.png";
//            ScreenshotUtil.savePic(ScreenshotUtil.takeScreenShot(this, deviceWidth, deviceHeight), path);
            camera.takePicture(null, null, ScreenshotUtil.mPicture);
            Toast.makeText(getApplicationContext(), "Screenshot Saved", Toast.LENGTH_SHORT).show();
        } catch (NullPointerException ignored) {
            ignored.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if(camera == null)
            getCameraDevice(holder);
        refreshCamera();
    }

    public void refreshCamera() {
        if (holder.getSurface() == null) {
            return;
        }
        try {
            camera.stopPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(5000);
            captureScreen();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        camera.release();
    }

    @SuppressLint("MissingPermission")
    private boolean getPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CAMERA
                            , Manifest.permission.WRITE_EXTERNAL_STORAGE
                            , Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length <= 0
                        && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    getPermissions();
                } else {
                    startActivity(new Intent(MainActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    MainActivity.this.finish();
                }
                return;
            }
        }
    }
}